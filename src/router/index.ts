import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
	{
		path: "/",
		redirect: "/home",
	},
	{
		path: "/home",
		name: "Home",
		component: Home,
	},
	{
		path: "/about",
		name: "About",
		component: () => import(/* webpackChunkName: "about" */ "../views/About.vue"),
	},
	{
		path: "/service",
		name: "Service",
		component: () => import(/* webpackChunkName: "service" */ "../views/Service.vue"),
	},
	{
		path: "/mall",
		name: "Mall",
		component: () => import(/* webpackChunkName: "mall" */ "../views/Mall.vue"),
	},
	{
		path: "/contact",
		name: "Contact",
		component: () => import(/* webpackChunkName: "mall" */ "../views/Contact.vue"),
	},
	{
		path: "/friends",
		name: "Friends",
		component: () => import(/* webpackChunkName: "mall" */ "../views/Friends.vue"),
	},
	{
		path: "/comment",
		name: "Comments",
		component: () => import(/* webpackChunkName: "mall" */ "../views/Comments.vue"),
	},
];

const router = new VueRouter({
	mode: "history",
	base: process.env.BASE_URL,
	routes,
});

export default router;
